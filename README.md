## GitLab SaaS Runners on macOS  - (Beta)

- Customers in GitLab Premium and Ultimate plans, and participants in the [GitLab for Open Source Program](https://about.gitlab.com/solutions/open-source/join/) are eligible to request access to the beta.


- To request access open an [issue](https://gitlab.com/gitlab-com/runner-saas-macos-limited-availability/-/issues/new) in this project and select the access_request template.

- The access review and onboarding process will typically take two business days.

- Once access is approved and setup, you can start building apps on the macOS Runners. Refer to the [getting started](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html) documentation for setup and use instructions.

- GitLab SaaS Runners on macOS [Limited Availability](https://docs.gitlab.com/ee/policy/alpha-beta-support.html#limited-availability-la) launch is now targeted for mid to late Q3 FY 2023.

- **Note** - The GitLab SaaS Runners on macOS are not available to customers in the Free plan.

